# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="A library that allows to determine at runtime what functions to call"
HOMEPAGE="http://www.dyncall.org"
SRC_URI="http://www.dyncall.org/r${PV}/${P}.tar.gz"
LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

DEPEND=""
RDEPEND="${DEPEND}"

src_configure() {
	# This is a simple shell-script that doesn't play nice with econf
	./configure --prefix="${D}/usr" || die
}

src_test() {
	emake tests
	emake run-tests
}
