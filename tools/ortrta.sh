#!/bin/bash

# Shell script to perform `git rebase -i <argument>`
# License: WTFPL2

errorout() {
	echo "Failed: $1"
	exit 1
}

repoman_this() {
	local gitdir=$(git rev-parse --show-toplevel)
	local ebuilds=( $(git diff --numstat HEAD^ | cut -f3 | grep '\.ebuild') )
	local dirs=()
	for i in ${ebuilds[@]}; do
		local dir=$(dirname ${i})
		local inIt=0
		for j in ${dirs[@]}; do
			if [[ "${j}" == "${dir}" ]]; then
				inIt=1
				break;
			fi
		done
		if [[ ${inIt} -eq 0 ]]; then
			dirs+="${dir}"
			pushd "${gitdir}/${dir}"
			repoman full
			popd 2>&1 > /dev/null
		fi
	done
	git add *
	git commit --amend --no-edit
	echo
	git rebase --continue 2>&1 | head -n1
}

choose() {
	while [[ true ]]; do
		echo
		echo -n "(e)dit, (r)epoman full affected dirs, (d)iff, (n)ext commit, exit (w)ith saving or e(x)it w/o saving? "
		read -n1 -r response
		
		printf '\r'
		tput el
		echo
		
		case ${response} in
			d )
				git diff --find-renames --find-copies --find-copies-harder --color=always HEAD^
				;;
			e )
				exit 0
				;;
			n )
				git rebase --continue 2>&1 | head -n1
				return
				;;
			r )
				repoman_this
				return
				;;
			w )
				for i in $(seq $(GIT_EDITOR='cat' git rebase --edit-todo | grep -v '^#' | wc -l) -1 0); do
					git rebase --continue 2>&1 | head -n1
					echo "left: ${i}"
				done
				;;
			x )
				echo "Aborting rebase..."
				git rebase --abort
				echo "Exiting."
				exit 0
				;;
			* )
				echo "Wrong key. Try again."
				;;
		esac
	done
}

resume() {
	count=$(GIT_EDITOR='cat' git rebase --edit-todo | grep -v '^#' | wc -l)
	while [[ ${count} -ge 0 ]]; do
		git diff --color --find-renames --find-copies --find-copies-harder --stat HEAD^ | cat
		echo "left: ${count}"
		count=$(( count - 1 ))
		choose
	done
	if [[ $(LC_ALL=C git rebase --edit-todo 2>&1 | \
						grep -v '^#\|No rebase in progress?' | \
						wc -l) \
				-eq 0 ]]; then
		echo "Done."
	else
		echo "Something went wrong here, there's still commits to process."
	fi
}

commence() {
	input=$1
	START=${input:=master}
	GIT_EDITOR='vim -c "%s/pick/edit/g | wq"' git rebase -i ${START} || errorout "git rebase -i ${START}"
	resume
}

STATUS=$(LC_ALL=C git status | head -n1 | grep -c 'interactive rebase in progress')

case ${STATUS} in
	1)
		resume
		;;
	0)
		commence $1
		;;
	*)
		errorout "Invalid status \"${STATUS}\"."
		;;
esac
