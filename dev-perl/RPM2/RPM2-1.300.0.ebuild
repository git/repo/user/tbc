# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DIST_AUTHOR="LKUNDRAK"
DIST_VERSION="1.3"
inherit perl-module

DESCRIPTION="Perl bindings for the RPM Package Manager API"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="app-arch/rpm
	virtual/perl-File-Path
	virtual/perl-File-Spec"
DEPEND="dev-perl/Module-Build"
