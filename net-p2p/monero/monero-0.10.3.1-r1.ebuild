# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit cmake-utils eutils systemd user

DESCRIPTION="A more secure, private, untraceable cryptocurrency"
HOMEPAGE="https://getmonero.org"
SRC_URI="https://github.com/monero-project/bitmonero/archive/v${PV}.tar.gz -> ${P}.tar.gz"
LICENSE="BSD"

SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="debug doc static test"

RDEPEND=">=net-dns/unbound-1.4.16
	>=dev-libs/libevent-2.0.22
	>=dev-cpp/gtest-1.5
	>=dev-libs/boost-1.58
	dev-libs/openssl:0[-bindist]"
DEPEND="${RDEPEND}
	>=dev-util/cmake-3
	>=sys-devel/gcc-4.7.3
	doc? ( app-doc/doxygen
		media-gfx/graphviz )
	test? ( >=dev-cpp/gtest-1.5 )"

pkg_setup() {
	enewgroup monero
	enewuser monero -1 -1 /var/lib/monero "monero"
}

src_configure() {
	local mycmakeargs=(
		-DBUILD_TESTS=$(usex test ON OFF)
		-DSTATIC=$(usex static ON OFF) )

	cmake-utils_src_configure
}

src_compile() {
	cmake-utils_src_configure

	use doc && HAVE_DOT=YES doxygen Doxyfile
}

src_install() {
	cmake-utils_src_install

	insinto /etc/monero
	doins "${FILESDIR}/monero.conf"
	fowners monero:monero /etc/monero/monero.conf
	fperms 600 /etc/monero/monero.conf

	newconfd "${FILESDIR}/monerod.conf" monerod
	newinitd "${FILESDIR}/monerod.runscript" monerod
	systemd_dounit "${FILESDIR}/monerod.service"

	keepdir /var/lib/monero/.bitmonero
	fperms  700 /var/lib/monero
	fowners monero:monero /var/lib/monero/
	fowners monero:monero /var/lib/monero/.bitmonero
	dosym   /etc/monero/monero.conf /var/lib/monero/.bitmonero/bitmonero.conf

	DOCS=( README.md )
	if use doc; then
		unset i
		for f in "${S}"/doc/html/*.html ; do
			HTML_DOCS[i++]="${f}"
		done
		for f in "${S}"/doc/html/*.png ; do
			HTML_DOCS[i++]="${f}"
		done
		for f in "${S}"/doc/html/*.js ; do
			HTML_DOCS[i++]="${f}"
		done
	fi
	einstalldocs
}
