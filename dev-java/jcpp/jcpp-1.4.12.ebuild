# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

JAVA_PKG_IUSE="doc source"
inherit java-pkg-2 java-pkg-simple vcs-snapshot

COMMIT="1119d798e56ca37f2f976f196e6c27f70f39508b"
DESCRIPTION="A complete, compliant, standalone Java implementation of the C preprocessor"
HOMEPAGE="https://github.com/shevek/jcpp"
SRC_URI="https://github.com/shevek/${PN}/archive/${COMMIT}.tar.gz -> jcpp-1.4.12.tar.gz"
LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

COMMON_DEP="dev-util/findbugs:0
	dev-java/ant-core:0
	dev-java/jopt-simple:0
	dev-java/jsemver:0
	dev-java/jsr305:0
	dev-java/slf4j-api:0"
RDEPEND=">=virtual/jre-1.7
	${COMMON_DEP}"
DEPEND=">=virtual/jdk-1.7
	${COMMON_DEP}"

S="${WORKDIR}/${P}"
JAVA_SRC_DIR="src/main/java"
JAVA_GENTOO_CLASSPATH="ant-core,findbugs,jopt-simple,jsemver,jsr305,slf4j-api"

src_install() {
	java-pkg-simple_src_install
	dodoc README.md
}
